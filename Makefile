# © 2007 maro (@macarony.de - Matthias Diener)
VERSION = 1.1

# edit PATH to customize installation
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

CC = cc

all: isbf

isbf: isbf.c
	@${CC} -Os -DVERSION=\"${VERSION}\" -o isbf isbf.c
	@strip $@

clean:
	@rm -f isbf isbf-${VERSION}.tar.gz

dist: clean
	@mkdir -p isbf-${VERSION}
	@cp -R Makefile README LICENSE.html isbf.c isbf.1 isbf-${VERSION}
	@tar -cf isbf-${VERSION}.tar isbf-${VERSION}
	@gzip isbf-${VERSION}.tar
	@rm -rf isbf-${VERSION}

install: all
	@echo install \"isbf\" to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f isbf ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/isbf
	@echo install man to ${DESTDIR}${MANPREFIX}/man1
	@mkdir -p ${DESTDIR}${MANPREFIX}/man1
	@sed 's/VERSION/${VERSION}/g' < isbf.1 > ${DESTDIR}${MANPREFIX}/man1/isbf.1
	@chmod 644 ${DESTDIR}${MANPREFIX}/man1/isbf.1

uninstall:
	@echo remove \"${BIN}\" from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/isbf
	@echo remove man from ${DESTDIR}${MANPREFIX}/man1
	@rm -f ${DESTDIR}${MANPREFIX}/man1/isbf.1

.PHONY: all clean dist install uninstall

