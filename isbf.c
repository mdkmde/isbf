/*
 * Copyright (c) 2006 marco (@macarony.de - Matthias Diener)
 * isbf - insert stream by fifo
 * See LICENSE.html file for license details.
 */

#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define BUFFER_SIZE 1024

char *fifo = NULL;

void clean_exit(int signum) {
	if (remove(fifo) != 0) 
		fprintf(stderr, "cannot remove fifo: %s\n", fifo);
	_exit(signum);
}

int main(int argc, char *argv[]) {
	int buflen = BUFFER_SIZE;
	char buf[buflen + 1];
	int len = 0;
	int fd;
	fd_set rd;
	if ((argc == 3) && (argv[1][0] == '-') && (argv[1][1] == 'f') && (argv[1][2] == 0)) {
		fifo = argv[2];
	} else if ((argc == 2) && (argv[1][0] == '-') && (argv[1][1] == 'v') && (argv[1][2] == 0)) {
		fputs("isbf-"VERSION", Copyright (c) marco (@macarony.de - Matthias Diener)\n", stdout);
		_exit(0);
	} else {
		fputs("usage: isbf [-v | -f fifo]\n", stderr);
		_exit(1);
	}
	if (mkfifo(fifo, 0600) != 0) {
		fprintf(stderr, "cannot create fifo: %s\n", fifo);
		_exit(1);
	}
	signal(SIGINT, clean_exit);
	signal(SIGTERM, clean_exit);
	signal(SIGPIPE, clean_exit);
	if ((fd = open(fifo, O_RDWR)) == -1) {
		fprintf(stderr, "cannot open fifo: %s\n", fifo);
		clean_exit(1);
	}
	while (1) {
		FD_ZERO(&rd);
		FD_SET(STDIN_FILENO, &rd);
		FD_SET(fd, &rd);
		if (select(fd + 1, &rd, NULL, NULL, NULL) == -1)
			clean_exit(0);
		if (FD_ISSET(STDIN_FILENO, &rd)) {
			if ((len = read(STDIN_FILENO, buf, buflen)) <= 0)
				clean_exit(0);
			if ((write(STDOUT_FILENO, buf, len)) == -1)
				clean_exit(0);
		}
		if (FD_ISSET(fd, &rd)) {
			if ((len = read(fd, buf, buflen)) <= 0)
				clean_exit(0);
			if ((write(STDOUT_FILENO, buf, len)) == -1)
				clean_exit(0);
		}
	}
	return(0);
}

